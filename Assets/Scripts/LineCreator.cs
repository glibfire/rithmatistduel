﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCreator : MonoBehaviour {

    public GameObject linePrefab;

    Line activeLine;

    private void Update() {
        //0 is left, 1 is middle, 2 is right mouse buttons
        if (Input.GetMouseButtonDown(0)) {
            GameObject lineGO = Instantiate(linePrefab);
            activeLine = lineGO.GetComponent<Line>();
        }

        if (Input.GetMouseButtonUp(0)) {
            activeLine = null;
        }

        if(activeLine != null) {

            // the Camera.main... converts mouse pos from screen location to location within game
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Debug.Log("mousePos: " + mousePos.ToString());
            activeLine.UpdateLine(mousePos);
        }

    }
}

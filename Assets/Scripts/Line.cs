﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour {

    public LineRenderer lineRenderer;
    public EdgeCollider2D edgeCol;

    List<Vector2> points;

    public void UpdateLine(Vector2 worldPos) {

        if (points == null) {
            points = new List<Vector2>();
            SetPoint(worldPos);
            return;
        }

        // Check if the mouse has moved enough for a new point
        // If it has, insert point at mouse position
        if (Vector3.Distance(points.Last(), worldPos) > .1f) {
            
            SetPoint(worldPos);
        }
        
    }
    
    void SetPoint(Vector2 point) {
        points.Add(point);

        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPosition(points.Count - 1, point);
        Debug.Log("Line point: " + point);
        if(points.Count > 1) {
            edgeCol.points = points.ToArray();
            for (int i = 0; i < edgeCol.points.Length; i++) {
                Debug.Log("Collider point: " + edgeCol.points[i].ToString());
            }
        }

    }
}
